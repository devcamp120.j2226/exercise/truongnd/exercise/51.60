package JAVABASIC;
import java.util.ArrayList;

import java.util.Arrays;
public class Array2 {
    public static void arrayToArrayList2(){
        Double[] arr = new Double[100];
        for (int i= 0;i< 100;i++){
            arr[i] = Double.valueOf(i+1);
        }
        ArrayList<Double> arrayList = new ArrayList<>(Arrays.asList(arr));
        System.out.println("Subtask 2");
        System.out.println(arrayList);
    }
    public static void main(String[] args) throws Exception {
        Array2.arrayToArrayList2();
    }
}
