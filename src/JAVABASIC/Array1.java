package JAVABASIC;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Array1 {
    public static void arrayToArrayList(){
        Long[] arr = new Long[100];
        for (int i= 0;i< 100;i++){
            arr[i] = Long.valueOf(i+1);
        }
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(arr));
        System.out.println(arrayList);
    }
    public static void main(String[] args) throws Exception {
        //Array1.arrayToArrayList();
    }
}